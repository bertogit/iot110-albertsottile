#!/usr/bin/python
import RPi.GPIO as GPIO
from debouncer import Debouncer

LED1_PIN = 18
LED2_PIN = 13
LED3_PIN = 23
SWT_PIN = 27

db = Debouncer()

#led_list = [LED1_PIN,LED2_PIN,LED3_PIN];

class IoTGPIO(object):

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LED1_PIN, GPIO.OUT)      # RED LED as output
        GPIO.setup(LED2_PIN, GPIO.OUT)      # GREEN LED as output
        GPIO.setup(LED3_PIN, GPIO.OUT)      # BLUE LED as output
        GPIO.setup(SWT_PIN, GPIO.IN)        # Switch as input
        GPIO.setup(SWT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.led_list = [LED1_PIN,LED2_PIN,LED3_PIN];

    def state(self, value):
        if(value == 1):
            return 'ON'
        else:
            return 'OFF'

    def set_led(self, led, value):
        GPIO.output(self.led_list[led], value)

    def get_led(self, led):
        #return self.state(GPIO.input(self.led_list[led]))
        return int(GPIO.input(self.led_list[led]))

    def get_switch(self):
        return (int(db.debounce(GPIO.input(SWT_PIN)))+1)%2
