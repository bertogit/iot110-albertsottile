$(document).ready(function() {

    var max_data_saved = 20;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];
    var sse_joystick_data

    // initialize joystick canvas
    init_canvas();
    //$('#tabs-joystick-tab').click(function(){ init_canvas(); return false; });

    // the key event receiver function
    iotSource.onmessage = function(e) {
        // parse sse received data
        parsed_json_data = JSON.parse(e.data);

        // Convert string datetime to JS Date object
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);
        //console.log(parsed_json_data);

        // Push new data to front of saved data list
        sse_sensor_data.unshift(parsed_json_data);

        // Then remove oldest data up to maximum data saved
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        updateEnvironmentalTable();
        update_env_chart();
        updateInertialTable();
        update_accel_chart();
        // update Joystick Data
        // update Dislay Data
    }

    iotStick.onmessage = function(e) {
        sse_joystick_data = JSON.parse(e.data);
        update_joystick();
    }

    // ============================ DATE FUNCTIONS ==============================
    // from http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
    function zeropad(num, size) {
        var s = "000000000" + num;
        return s.substr(s.length - size);
    }

    function getDateNow() {
        var d = new Date();
        var date = (d.getFullYear()) + '-' + d.getMonth() + 1 + '-' + d.getDate();
        var time = zeropad(d.getHours(), 2) + ':' + zeropad(d.getMinutes(), 2) +
            ':' + zeropad(d.getSeconds(), 2) + '.' + zeropad(d.getMilliseconds(), 3);
        return { time: time, date: (date + " " + time) };
    }

    // ============================ DATE FUNCTIONS ==============================

    // ============================== ENV TABLE =================================
    function updateEnvironmentalTable() {
        $('tr.env-param-row').each(function(i) {
            var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['humidity'].value.toFixed(2) + '</td>';
            $(this).html(new_html);
        });
    }
    // ============================== ENV TABLE =================================

    // ============================ INERTIAL TABLE ==============================
    function updateInertialTable() {
      $('tr.imu-param-row').each(function(i) {
          var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['x'].toFixed(2) + '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['y'].toFixed(2) + '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['z'].toFixed(2)+ '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['pitch'].toFixed(4)+ '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['roll'].toFixed(4)+ '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['yaw'].toFixed(2)+ '</td>';
          new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['compass'].toFixed(2)+ '</td>';
          $(this).html(new_html);
      });
    }

    // ============================ Clear Table Rows ==============================

    function clearTableRow(tableRowName) {
        $('tr#' + tableRowName).each(function(i) {
            $(this).empty();
        });
    }

    // ============================== ENV CHART ================================
    // initialize the accel chart structure
    var env_chart = new Morris.Line({
        element: 'env-chart',
        data: '',
        xkey: 'time',
        ykeys: ['humidity', 'temp'],
        labels: ['%RH', '&degC']
    });

    // build the environmental chart data array for MorrisJS structure
    function update_env_chart() {
        var chart_data = [];
        sse_sensor_data.forEach(function(d) {
            env_record = {
                'time': d['meas_time'].getTime(),
                'humidity': d['environmental']['humidity'].value,
                'temp': d['environmental']['temperature'].value
            };
            //console.log(env_record);
            chart_data.push(env_record);
        });
        //console.log(chart_data);
        env_chart.setData(chart_data);
    };
    // ============================== ENV CHART ================================

    // ============================== ACCEL CHART ================================
    var accel_chart = new Morris.Line({
        element: 'accel-chart',
        data: [],
        xkey: 'time',
        ykeys: ['x', 'y', 'z'],
        labels: ['Accel-X', 'Accel-Y', 'Accel-Z']
    });

    // build the chart data array for MorrisJS structure
    function update_accel_chart() {
        var chart_data = [];
        // var i = 0;
        sse_sensor_data.forEach(function(d) {
            accel_record = {
                'time': d['meas_time'].getTime(),
                'x': d['inertial']['accelerometer']['x'],
                'y': d['inertial']['accelerometer']['y'],
                'z': d['inertial']['accelerometer']['z']
            };
            chart_data.push(accel_record);
        });
        accel_chart.setData(chart_data);
    };
    // ============================== ACCEL CHART ================================

    // ============================== JOYSTICK CANVAS ================================
    function init_canvas()
    {
        // initialize joystick canvas
        canvas = $('#joystickcanvas');
        context = canvas[0].getContext("2d");
        parent = $('#tabs-joystick-data');

        width =50; height = 50;   // img dimensions
        steps = 5;
        img = new Image();
        img.src = "/static/img/rpi.png";

        window.onresize = resizeCanvas;
        window.onload = resizeCanvas;
        resizeCanvas();
    }

    function resizeCanvas(){
        canvas.attr('width',  parent.outerWidth());
        canvas.attr('height',  parent.outerHeight());
        x = context.canvas.width / 4;
        y = context.canvas.height / 4;
        render();
    }


    function update_joystick()
    {
        direction = sse_joystick_data['joystick']['direction']
        status = sse_joystick_data['joystick']['status']

        if(status!='up')
        {
          switch(direction)
          {
              case 'KEY_UP':
                y -= steps;
              break;

              case 'KEY_DOWN':
                y += steps;
              break;

              case 'KEY_LEFT':
                x -= steps;
              break;

              case 'KEY_RIGHT':
                x += steps;
              break;
          }
          render();
        }
    }

    function render()
    {
        context.clearRect(0, 0, context.canvas.width, context.canvas.height);
        context.drawImage(img, x, y, width, height);
    }













});
