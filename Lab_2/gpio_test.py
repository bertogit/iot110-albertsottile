#!/usr/bin/python
import time
from gpio import IoTGPIO
#from gpio import led_list

pigpio = IoTGPIO()
while True:
    print('\n __________')

    switch = pigpio.get_switch()
    print('SWT: {0}'.format(pigpio.state(switch)))

    for num in range(len(pigpio.led_list)):
        pigpio.set_led(num, True)
        print('LED{0}: {1}'.format(num+1,pigpio.get_led(num)))
        time.sleep(2)
        pigpio.set_led(num, False)
        print('LED{0}: {1}'.format(num+1,pigpio.get_led(num)))
    time.sleep(1)
