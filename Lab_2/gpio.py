#!/usr/bin/python
import RPi.GPIO as GPIO

LED1_PIN = 18
LED2_PIN = 13
LED3_PIN = 23
SWT_PIN = 27

#led_list = [LED1_PIN,LED2_PIN,LED3_PIN];

class IoTGPIO(object):

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LED1_PIN, GPIO.OUT)      # RED LED as output
        GPIO.setup(LED2_PIN, GPIO.OUT)      # GREEN LED as output
        GPIO.setup(LED3_PIN, GPIO.OUT)      # BLUE LED as output
        GPIO.setup(SWT_PIN, GPIO.IN)        # Switch as input
        GPIO.setup(SWT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.led_list = [LED1_PIN,LED2_PIN,LED3_PIN];

    def state(self, value):
        if(value == 1):
            return 'ON'
        else:
            return 'OFF'

    def set_led(self, led, value):
        GPIO.output(self.led_list[led], value)

    def get_led(self, led):
        return self.state(GPIO.input(self.led_list[led]))

    def get_switch(self):
        switch = GPIO.input(SWT_PIN)
        if (switch == 0):
            switch = 1
        else:
            switch = 0
        return switch
