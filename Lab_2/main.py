#!/usr/bin/python
from flask import *
from gpio import IoTGPIO

app = Flask(__name__)
pigpio = IoTGPIO()

@app.route("/")
def index():
    # create an instance of my pi gpio object class.
    pigpio = IoTGPIO()
    switch_state = pigpio.get_switch()
    led1_state = pigpio.get_led(0)
    led2_state = pigpio.get_led(1)
    led3_state = pigpio.get_led(2)
    return render_template('index.html', switch=switch_state,
                                led1=led1_state,
                                led2=led2_state,
                                led3=led3_state)

# curl http://192.168.86.95:5000/led/1
@app.route("/led/<int:led_state>", methods=['GET'])
def leds(led_state):
    return "LED State:" + str(pigpio.get_led(led_state-1)) + "\n"

# curl http://192.168.86.95:5000/sw
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pigpio.get_switch()) + "\n"

# curl --data 'led=1' http://192.168.86.95:5000/ledtoggle
@app.route("/ledtoggle", methods=['POST'])
def ledtoggle():
    led = int(str(request.form['led']))
    if (pigpio.get_led(led-1) == 'ON'):
        pigpio.set_led(led-1,0)
    else:
        pigpio.set_led(led-1,1)
    return "LED Toggle: " + str(led)

# curl --data 'led=1&state=ON' http://192.168.86.95:5000/ledcmd
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pigpio.set_led(led-1,False)
    elif (state == 'ON'):
        pigpio.set_led(led-1,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
