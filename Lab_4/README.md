# IoT110 - Lab 4
## Albert Sottile

* stand alone app for reading *Bosch BMP280* Pressure/Temperature sensor
* setup RPI for using I2C serial bus protocol ( *Inter-Integrated Circuit* )
* use **Matt Hawkins** Python Driver for the BMP280
* create unit tests to ensure the following for the BMP280:
  1. It is online
  2. It is wired correctly
  3. It is providing reasonably accurate readings for temperature and pressure

---

1. **bmp280.py** - the python driver for the BMP280
2. **bmp280_test.py** - a unit test file for the python driver.  Request its ID and its current readings for temperature and pressure.
