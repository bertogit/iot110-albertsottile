# IoT110 - Lab 3
## Albert Sottile

* continuation of Lab_2
* add Server Side Events (SSE)
* add better styling with Bootstrap and more JavaScript

---

1. **gpio.py** - Simplified some methods and removed the *state* return type.
2. **debouncer.py** - A class to prevent changes in switch logic until several program cycles have passed; stabilizes true switch feedback.
3. **main.py** - Used with web page *index.html*.  Add debouncer for switch.  Add method to handle *myData* which is the data from the SSE message.  Update switch and led feedback on the webpage.
4. **index.html** - Move style and JavaScript to external files.  Add Bootstrap and JQuery.  Improve GUI layout.  Add Server Side Events (SSE) to send constant messages to jQuery method.  Add colors to webpage LED feedback.
5. **main.js** - My JavaScript file.  Process event data and update switch and led feedback.  
6. **main.css** - My CSS stylesheet.  A bit of styling for the page.
