#!/usr/bin/python
import time
import datetime
from flask import *
from gpio import IoTGPIO
from notify import Notify

app = Flask(__name__)
pigpio = IoTGPIO()
notifyOpt = Notify()

# ============================== Functions ===================================

# ============================== API Routes ===================================
@app.route("/")
def index():
    return render_template('index.html')

# curl http://192.168.86.95:5000/sens
@app.route("/sens", methods=['GET'])
def sens():
  return "Sensor State:" + pigpio.get_sensor() + "\n"

# curl http://192.168.86.95:5000/myData
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            data_obj = {}
            data_obj["notified"] = "no"
            data_obj["meas_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S");
            data_obj["sensor"] = pigpio.get_sensor()
            neednotify = pigpio.check_state(data_obj["sensor"])
            data_obj["curr_state"] = pigpio.get_state()
            if neednotify:
                notifyOpt.email(data_obj,data_obj["curr_state"])
                data_obj["notified"] = "notified"

            yield('data: {0}\n\n'.format(json.dumps(data_obj)))
            time.sleep(1.0)

    return Response(get_state_values(), mimetype='text/event-stream')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
