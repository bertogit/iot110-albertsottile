# IoT110 - Lab 2
## Albert Sottile

* using GPIO, LEDs and Switches via Python and WebApp
* using the flask web framework

---

1. **gpio.py** - Create a class IoTGPIO that contains methods and properties for interfacing with the RPi GPIO and the circuit of 3 LEDs and a button (switch).
2. **gpio_test.py** - Create an instance of the class and test the methods.  Confirm the LEDs turn on and correct state is reported to the command line.
3. **api_server.py** - A Flask server to test GET and POST requests from the command line via `curl`.
4. **main.py** - Used with web page *index.html*.  Create an instance of the gpio class, create a Flask server, use GET and POST to access the gpio methods.  Turn on LEDs and show the current state on the web page.
5. **index.html** - Use jquery to handle html button click events.  Call routines that access the gpio methods for turning on LEDs and reading the current state.  Write results to the web console.  Use `<span>` tag and `{{ }}` to dynamically update LED state on the web page instead of needing to refresh.
