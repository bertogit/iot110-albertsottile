
const RED = 0;
const GRN = 1;
const BLU = 2;

var ledBtn = $(".led_row .led_button");
var ledLabel = $(".led_row .label");
var labelColor = ['label-danger','label-success','label-primary'];
var ledNames = ['led_red','led_grn','led_blu']
var led = new Array(3);

var cached_sensor_readings = [];
var tempGraph = new Morris.Line({
      element: 'mytempchart',
      data: [],
      xkey: 'time',
      ykeys: ['temp'],
      labels: ['Temperature (C)']
});

var pressGraph = new Morris.Line({
      element: 'mypresschart',
      data: [],
      xkey: 'time',
      ykeys: ['press'],
      labels: ['Pressure (hPa)']
});

  $(document).ready(function() {
    iotSource.onmessage = function(e) {
      //console.log(e.data);
      sse_data = JSON.parse(e.data);
      //console.log(sse_data);

      updateSwitch(sse_data['switch']);
      for (i = 1; i < led.length+1; i++) {
        updateLeds(i,sse_data[ledNames[i-1]]);
      }

      cached_sensor_readings.unshift({
        'time': sse_data['meas_time'],
        'temp': sse_data['temperature']['reading'],
        'press': sse_data['pressure']['reading']
      });
      while (cached_sensor_readings.length > 10) cached_sensor_readings.pop();

      updateDataTable();
      updateTempChart();
      updatePressChart();
    }
  });

  function updateTempChart() {
    tempGraph.setData(cached_sensor_readings);
  }

  function updatePressChart() {
    pressGraph.setData(cached_sensor_readings);
  }

  function updateDataTable() {
    rStr = "";
    cached_sensor_readings.slice(0, 5).forEach(function(di) {
      rStr += "<tr>";
      rStr += "<td>" + di['time'] + "</td>";
      rStr += "<td>" + di['temp'] + "</td>";
      rStr += "<td>" + di['press'].toFixed(4) + "</td>";
      rStr += "</tr>";
    });
    $("tbody#sensor-data").html(rStr);
  }

  /* update the Switch based on its SSE state monitor */
  function updateSwitch(switchValue) {
    //$('#switch').text(switchValue);
    if(switchValue === "ON") {
      $('#switch').toggleClass( 'label-default', false);
      $('#switch').toggleClass( labelColor[GRN], true);
    }
    else {
      $('#switch').toggleClass( 'label-default', true);
      $('#switch').toggleClass( labelColor[GRN], false);
    }
  }

  /* update the LEDs based on their SSE state monitor */
  function updateLeds(ledNum,ledValue) {
    //$(ledLabel[ledNum-1]).text(ledValue);
    if(ledValue === "ON") {
      $(ledLabel[ledNum-1]).toggleClass( 'label-default', false);
      $(ledLabel[ledNum-1]).toggleClass( labelColor[ledNum-1], true);
    }
    else {
      $(ledLabel[ledNum-1]).toggleClass( 'label-default', true);
      $(ledLabel[ledNum-1]).toggleClass( labelColor[ledNum-1], false);
    }
  }

  $(ledBtn).click(function() {
    var i = ledBtn.index(this);
    if (led[i] === "OFF") {led[i] = "ON";} else {led[i] = "OFF";}
    var params = 'led='+(i+1)+'&state='+led[i];
    console.log('Led Command with params:' + params);
    $.post('/ledcmd', params, function(data, status){
      console.log("Data: " + data + "\nStatus: " + status);
      });
    });















//
