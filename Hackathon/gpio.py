#!/usr/bin/python
import RPi.GPIO as GPIO
from debouncer import Debouncer

SENS_PIN = 27
db = Debouncer()

# pins connected to the SPI port on the ADC
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

fsr_adc = 0     # force sensitive resistor connected to adc channel #0
min_value = 50  # min_value to register delivered or removed (range is 0..1000)

class IoTGPIO(object):

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(SENS_PIN, GPIO.IN)
        GPIO.setup(SENS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(SPIMOSI, GPIO.OUT)
        GPIO.setup(SPIMISO, GPIO.IN)
        GPIO.setup(SPICLK, GPIO.OUT)
        GPIO.setup(SPICS, GPIO.OUT)
        self.curr_state = "removed"

    def check_state(self,value):
        update = False
        if value > min_value and self.curr_state == "removed":
            self.curr_state = "delivered"
            update = True
            #print(self.curr_state)
        elif value < min_value and self.curr_state == "delivered":
            self.curr_state = "removed"
            update = True
            #print(self.curr_state)
        return update

    def get_state(self):
        return self.curr_state

    def get_sensor(self):
        return self.read_adc(fsr_adc,SPICLK, SPIMOSI, SPIMISO, SPICS)
        #return db.debounce(GPIO.input(SENS_PIN))

    # https://learn.adafruit.com/reading-a-analog-in-and-controlling-audio-volume-with-the-raspberry-pi/script
    # read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
    def read_adc(self, adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)

        adcout >>= 1       # first bit is 'null' so drop it
        return adcout
