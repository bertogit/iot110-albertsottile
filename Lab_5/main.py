#!/usr/bin/python
import time
import datetime
from flask import *
from gpio import IoTGPIO
from bmp280 import PiBMP280

app = Flask(__name__)
pigpio = IoTGPIO()

# create an array of my pi bmp280 sensor dictionaries
sensor = {"name" : "bmp280", "addr" : 0x76, "chip" : PiBMP280(0x76) , "data" : {}}
(chip_id, chip_version) = sensor["chip"].readBMP280ID()
sensor["data"]["chip_id"] = chip_id
sensor["data"]["chip_version"] = chip_version


# ============================== Functions ====================================
def get_sensor_values():
    (temperature, pressure) = sensor["chip"].readBMP280All()
    sensor["data"]["temperature"] = { "reading": temperature, "units" : "C" }
    sensor["data"]["pressure"] = { "reading": pressure, "units" : "hPa"
}
    return sensor["data"]


# ============================== API Routes ===================================
@app.route("/")
def index():
    return render_template('index.html')

# curl http://192.168.86.95:5000/led/1
@app.route("/led/<int:led_state>", methods=['GET'])
def leds(led_state):
    return "LED State:" + pigpio.state(pigpio.get_led(led_state-1)) + "\n"

# curl http://192.168.86.95:5000/sw
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + pigpio.state(pigpio.get_switch()) + "\n"

# curl --data 'led=1' http://192.168.86.95:5000/ledtoggle
@app.route("/ledtoggle", methods=['POST'])
def ledtoggle():
    led = int(str(request.form['led']))
    if (pigpio.state(pigpio.get_led(led-1)) == 'ON'):
        pigpio.set_led(led-1,0)
    else:
        pigpio.set_led(led-1,1)
    return "LED Toggle: " + str(led)

# curl --data 'led=1&state=ON' http://192.168.86.95:5000/ledcmd
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    print "LED Command: " + request.data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pigpio.set_led(led-1,False)
    elif (state == 'ON'):
        pigpio.set_led(led-1,True)
    else:
        return "Argument Error"
    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"

# curl http://192.168.86.95:5000/myData
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            data_obj = get_sensor_values()
            data_obj["meas_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S");
            data_obj["led_red"] = pigpio.state(pigpio.get_led(0))
            data_obj["led_grn"] = pigpio.state(pigpio.get_led(1))
            data_obj["led_blu"] = pigpio.state(pigpio.get_led(2))
            data_obj["switch"] = pigpio.state(pigpio.get_switch())
            yield('data: {0}\n\n'.format(json.dumps(data_obj)))
            time.sleep(1.0)
    return Response(get_state_values(), mimetype='text/event-stream')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
