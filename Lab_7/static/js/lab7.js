$(document).ready(function() {

    var max_data_saved = 4;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];

    // the key event receiver function
    iotSource.onmessage = function(e) {
        // parse sse received data
        // console.log(e.data);
        var parsed_json_data = JSON.parse(e.data);

        // For now just log to console
        // console.log(parsed_json_data);

        // Convert string datetime to JS Date object
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);
        //console.log(parsed_json_data);

        // Push new data to front of saved data list
        sse_sensor_data.unshift(parsed_json_data);

        // Then remove oldest data up to maximum data saved
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        updateEnvironmentalTable();
        //updateSwitchIndicator();
        updateStepperMotor();
    }

    // Buttons
    $('#motor_start').click(function() {
        console.log('Start Motor Up!');
        $.get('/motor/1');
    });
    $('#motor_stop').click(function() {
      console.log('Stop Motor');
      $.get('/motor/0');
    });
    $('#motor_zero').click(function() {
      console.log('Zero Motor Position');
      $.get('/motor_zero');
    });
    $('#motor_multistep').click(function() {
      var params = 'steps='+$('#motor_steps').val()+"&direction="+$('#motor_direction').val();
      console.log('Multistep with params:' + params);
      $.post('/motor_multistep', params, function(sse_sensor_data, status){
                  console.log("Data: " + sse_sensor_data + "\nStatus: " + status);
              });
    });

    // Text Fields
    $('#motor_speed').change(function() {
      console.log('Changed motor speed to ' + $('#motor_speed').val());
      $.get('/motor_speed/'+$('#motor_speed').val());
    });
    $('#motor_position').change(function() {
      console.log('Changed motor position to ' + $('#motor_position').val());
      $.get('/motor_position/'+$('#motor_position').val());
    });

    $('#motor_steps').change(function() {
      console.log('Changed motor steps to ' + $('#motor_steps').val());
      $.get('/motor_steps/'+$('#motor_steps').val());
    });

    $('#motor_direction').change(function() {
      console.log('Changed motor steps to ' + $('#motor_direction').val());
      $.get('/motor_direction/'+$('#motor_direction').val());
    });

    // ============================ STEPPER MOTOR ===============================
    function updateStepperMotor() {
      $('#motor_position').html(sse_sensor_data[0]['motor']['position']);
      if (sse_sensor_data[0]['motor']['state'] === '1') {
        $('#motor_state').toggleClass('label-default', false);
        $('#motor_state').toggleClass('label-success', true);
      } else if (sse_sensor_data[0]['motor']['state'] === '0') {
        $('#motor_state').toggleClass('label-default', true);
        $('#motor_state').toggleClass('label-success', false);
      }
    }

    // ============================ STEPPER MOTOR ===============================

    // ============================== ENV TABLE =================================

    function updateEnvironmentalTable() {
        $('tr.env-param-row').each(function(i) {
            var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].reading.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].reading.toFixed(2) + '</td>';
            $(this).html(new_html);
        });
    }

    // ============================== / ENV TABLE ===============================

    // Renders the jQuery-ui elements
    $("#tabs").tabs();

    // ===================================================================
    // LED 1 SLIDER
    $("#slider1").slider({
      orientation: "vertical",
      min: 0,
      max: 100,
      value: 50,
      animate: true,
      slide: function(event, ui) {
          var dcValue = ui.value;
          $("#pwm1").val(dcValue);
          console.log("red led duty cycle(%):", ui.value);
          $.post('/set_pwm', { pwmChan:0, dutyCycle: ui.value });
      }
    });
    $("#pwm1").val($("#slider1").slider("value"));

    // ===================================================================
    // LED 2 SLIDER
    $("#slider2").slider({
      orientation: "vertical",
      min: 0,
      max: 100,
      value: 50,
      animate: true,
      slide: function(event, ui) {
          var dcValue = ui.value;
          $("#pwm2").val(dcValue);
          console.log("blue led duty cycle(%):", ui.value);
          $.post('/set_pwm', { pwmChan:1, dutyCycle: ui.value });
      }
    });
    $("#pwm2").val($("#slider2").slider("value"));

    // ===================================================================

});
