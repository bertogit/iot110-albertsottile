#!/usr/bin/python

import time
from gpio import PiGpio
piio = PiGpio()

print("Blinking LEDs and checking switch (^C to stop)\n")
while True:
    print "LED: ",
    for ledID in range(1, 4):
        print "{0}".format(ledID),
        piio.set_led(ledID, True)
        time.sleep(0.3)
        piio.set_led(ledID, False)
        time.sleep(0.3)
        piio.toggle_led(ledID)
        time.sleep(0.1)
        piio.toggle_led(ledID)
        time.sleep(0.1)
    print(" Sw: {0}".format(piio.get_switch()))
    time.sleep(0.2)

