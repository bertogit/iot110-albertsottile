
var cached_sensor_readings = [];

var sensorGraph = new Morris.Line({
      element: 'sensorchart',
      data: [],
      xkey: 'time',
      ykeys: ['sensor'],
      labels: ['Sensor']
});

  $(document).ready(function() {
    iotSource.onmessage = function(e) {
      sse_data = JSON.parse(e.data);
      console.log(sse_data);

      cached_sensor_readings.unshift({
        'time': sse_data['meas_time'],
        'sensor': sse_data['sensor']
      });
      while (cached_sensor_readings.length > 10) cached_sensor_readings.pop();

      updateSensorChart();
      updateSensor(sse_data['sensor']);
      shownotify(sse_data['curr_state'],sse_data['notified']);
    }

    $('.notification').hide();
  });

  function updateSensorChart() {
    sensorGraph.setData(cached_sensor_readings);
  }

  function updateSensor(value) {
    measured_voltage = (3.3/1023)*value;
    //console.log("Voltage =",measured_voltage);
  }

  function shownotify(state,notified)
  {
    if(notified=='notified')
    {
      $('.notification #notificationtext')
        .text('Package has been ' + state+'. Notification has been sent');

      $('.notification').fadeIn(1000);
      $('.headertext').hide();
      plantinfoTimeout = setTimeout(function () {
         $('.notification').hide();
         $('.headertext').fadeIn(1000);
       }, 5000);
    }
  }
