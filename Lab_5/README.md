# IoT110 - Lab 5
## Albert Sottile

* combine Lab_3: *GPIO* and Lab_4: *temp & pressure*
* use Morris.js to create a plat of temperature and pressure
* a bit of page rearranging

---

1. **gpio.py** - interact with the three LEDs and the switch on the breadboard.  Moved debouncer code to *gpio.py* to ensure info is accurate before leaving the module.
3. **main.py** - Used with web page *index.html*.  Changed to pass object to *myData* instead of a string.  Added more methods to handle BMP280 data.
4. **index.html** - added table to show the temperature and pressure in a graph
5. **bmp280.py** - the python driver for the BMP280
6. **main.js** - My JavaScript file.  creates objects for tempGraph and tempCHart.
7. **main.css** - My CSS stylesheet.  A bit of styling for the page.
