# IoT110 - Hackathon Project
## A Package Delivery Notification System - by Albert Sottile

* Using a FSR (Force Sensitive Resistor); Ribbon style 2 feet long.
* Using a MCP3008 ADC converter chip to read analog values of the resistor. [Link to MCP3008 on Adafruit](https://learn.adafruit.com/reading-a-analog-in-and-controlling-audio-volume-with-the-raspberry-pi/connecting-the-cobbler-to-a-mcp3008)
* When a package is placed on the FSR, a "Delivered" notification is sent to an email recipient.
* When the package is removed, a "Removed" notification is sent to an email recipient.
* Wiring example and code to read from the ADC are shown at:

---

1. **gpio.py** - a class to handle the GPIO, reading from the ADC, and reading from the FSR
2. **notify.py** - a class to handle email notification.
