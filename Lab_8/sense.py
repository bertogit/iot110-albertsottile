#!/usr/bin/python
# =============================================================================
#        File : sense.py
# Description : Driver interface for SenseHat Module
#      Author : S. Dame
#        Date : 2/13/2017
# =============================================================================
#
#  Official sense-hat API available from :
#  http://pythonhosted.org/sense-hat/api/
#
# =============================================================================
from sense_hat import SenseHat
from datetime import datetime
import pprint
import time
import evdev

class PiSenseHat(object):
    """Raspberry Pi 'IoT Sense Hat API Driver Class'."""

    # Constructor
    def __init__(self):
        self.sense = SenseHat()
        # enable all IMU functions
        self.sense.set_imu_config(True, True, True)
        self.device = evdev.InputDevice('/dev/input/event2')
        #print(self.device)
        self.keystates = ['up', 'down', 'hold']

    # pixel display
    def set_pixel(self,x,y,color):
        # red = (255, 0, 0)
        # green = (0, 255, 0)
        # blue = (0, 0, 255)
        self.sense.set_pixel(x, y, color)

    # clear pixel display
    def clear_display(self):
        self.sense.clear()

    # Pressure
    def getPressure(self):
        return self.sense.get_pressure()

    # Temperature
    def getTemperature(self):
        return self.sense.get_temperature()

    # Humidity
    def getHumidity(self):
        return self.sense.get_humidity()

    def getHumidityTemperature(self):
        return self.sense.get_temperature_from_humidity()

    def getPressureTemperature(self):
        return self.sense.get_temperature_from_pressure()

    def getOrientationRadians(self):
        return self.sense.get_orientation_radians()

    def getOrientationDegrees(self):
        return self.sense.get_orientation_degrees()

    # degrees from North
    def getCompass(self):
        return self.sense.get_compass()

    def getAccelerometer(self):
        return self.sense.get_accelerometer_raw()

    def getEnvironmental(self):
        sensors = {'name' : 'sense-hat', 'environmental':{}}
        return sensors

    def getJoystick(self):
        keycode = ' '; keystate=' '
        sensors = {'direction':{}, 'status':{}}

        try:
            evt = self.device.read_one()
            if evt.type == evdev.ecodes.EV_KEY:
                catevt = evdev.categorize(evt)
                keycode=catevt.keycode; keystate=self.keystates[catevt.keystate]
                #print(evdev.categorize(evt))

        except Exception as e:
            keycode = 'none'; keystate='none'

        sensors['direction'] = keycode
        sensors['status'] = keystate
        return sensors

    # Joystick loop - run in main as background process (incomplete)
#    def getJoystickLoop(self):
#        for evt in self.device.read_loop():
#             if evt.type == evdev.ecodes.EV_KEY:
#                 print(evdev.categorize(evt))

    def getInertial(self):
        sensors = {'name' : 'sense-hat', 'inertial':{}}

    def getAllSensors(self):
        sensors = {'name' : 'sense-hat', 'environmental':{}, 'inertial':{}, 'joystick':{}}
        sensors['environmental']['pressure'] = { 'value':self.sense.get_pressure(), 'unit':'mbar'}
        sensors['environmental']['temperature'] = { 'value':self.sense.get_temperature(), 'unit':'C'}
        sensors['environmental']['humidity'] = { 'value':self.sense.get_humidity(), 'unit': '%RH'}
        accel = self.sense.get_accelerometer_raw()
        sensors['inertial']['accelerometer'] = { 'x':accel['x'], 'y':accel['y'], 'z': accel['z'], 'unit':'g'}
        orientation = self.sense.get_orientation_degrees()
        sensors['inertial']['orientation'] = { 'compass':self.sense.get_compass(), 'pitch':orientation['pitch'], 'roll':orientation['roll'], 'yaw': orientation['yaw'], 'unit':'degrees'}
        #sensors['joystick'] = self.getJoystick()         # need to comment because it grabs the event before other methods can use it
        sensors['meas_time'] = datetime.now().isoformat()
        return sensors
# =============================================================================
# main to test from CLI
def main():
    # create an instance of my pi sense-hat sensor object
    pi_sense_hat = PiSenseHat()

    # Read Parameters.
    p = pi_sense_hat.getPressure()
    t_c = pi_sense_hat.getTemperature()
    h = pi_sense_hat.getHumidity()
    ht = pi_sense_hat.getHumidityTemperature()
    hp = pi_sense_hat.getPressureTemperature()
    orientation = pi_sense_hat.getOrientationDegrees()
    accel = pi_sense_hat.getAccelerometer()
    d = pi_sense_hat.getCompass()
    #j = pi_sense_hat.getJoystick()         # need to comment because it grabs the event before other methods can use it

    print("================ Discrete Sensor Values ==================")
    print "      Pressure :", p
    print "   Temperature :", t_c
    print "      Humidity :", h
    print "  HumidityTemp :", ht
    print "  PressureTemp :", hp
    print "       Compass :", d
    print("  p: {pitch}, r: {roll}, y: {yaw}".format(**orientation))
    print("  x: {x}, y: {y}, z: {z}".format(**accel))
    print("       JoyStick: ", j)
    print("==========================================================\n")

    print("================== Dictionary Object =====================")
    pprint.pprint(pi_sense_hat.getAllSensors())
    print("==========================================================\n")


if __name__=="__main__":
    main()
