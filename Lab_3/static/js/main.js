
const GRN = 1;

var ledBtn = $(".led_row .led_button");
var ledLabel = $(".led_row .label");
var labelColor = ['label-danger','label-success','label-primary'];
var led = new Array(3);

  $(document).ready(function() {
    iotSource.onmessage = function(e) {
      //console.log(e.data);
      var params = e.data.split(' ');
      updateSwitch(params[0]);
      for (i = 1; i < led.length+1; i++) {
        updateLeds(i,params[i]);
      }
    }
  });

  /* update the Switch based on its SSE state monitor */
  function updateSwitch(switchValue) {
    //$('#switch').text(switchValue);
    if(switchValue === "ON") {
      $('#switch').toggleClass( 'label-default', false);
      $('#switch').toggleClass( labelColor[GRN], true);
    }
    else {
      $('#switch').toggleClass( 'label-default', true);
      $('#switch').toggleClass( labelColor[GRN], false);
    }
  }

  /* update the LEDs based on their SSE state monitor */
  function updateLeds(ledNum,ledValue) {
    //$(ledLabel[ledNum-1]).text(ledValue);
    if(ledValue === "ON") {
      $(ledLabel[ledNum-1]).toggleClass( 'label-default', false);
      $(ledLabel[ledNum-1]).toggleClass( labelColor[ledNum-1], true);
    }
    else {
      $(ledLabel[ledNum-1]).toggleClass( 'label-default', true);
      $(ledLabel[ledNum-1]).toggleClass( labelColor[ledNum-1], false);
    }
  }

  $(ledBtn).click(function() {
    var i = ledBtn.index(this);
    if (led[i] === "OFF") {led[i] = "ON";} else {led[i] = "OFF";}
    var params = 'led='+(i+1)+'&state='+led[i];
    console.log('Led Command with params:' + params);
    $.post('/ledcmd', params, function(data, status){
      console.log("Data: " + data + "\nStatus: " + status);
      });
    });















//
